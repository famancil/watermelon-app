import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../start';

const md5 = require('md5');
const redis = require('redis');

const client = redis.createClient({
    host: process.env.REDIS_URL,
    port: 6379
});

// Configurar las pruebas
chai.use(chaiHttp);
chai.should();

describe("Home", () => {
    describe("GET /", () => {

        it("should get Home", (done) => {
             chai.request(app)
                 .get('/')
                 .end((err, res) => {
                     res.should.have.status(200);
                     done();
                  });
        });
    });
});


describe("Users", () => {

    describe("GET /users", () => {
       
        it("should get new user record", (done) => {

             chai.request(app)
                 .post(`/users`)
                 .set('content-type', 'application/json')
                 .send({
                        "username": "sampleAdmin",
                        "email": "sampleAdmin@gmail.com",
                        "password":"sampleAdmin"
                    })
                 .end((err, res) => {
                     res.should.have.status(200);
                     res.body.should.be.a('object');
                     done();
                  });
        });

        it("should get error in new user", (done) => {

             chai.request(app)
                 .post(`/users`)
                 .set('content-type', 'application/json')
                 .send({
                        "username": "sampleAdmin",
                        "email": "sampleAdmin@gmail.com",
                        "password":"sampleAdmin"
                    })
                 .end((err, res) => {
                     res.should.have.status(400);
                     res.body.should.be.a('object');
                     done();
                  });
        });

        it("should get all users record", (done) => {
            
             chai.request(app)
                 .get(`/users`)
                 .end((err, res) => {
                     res.should.have.status(200);
                     res.body.should.be.a('array')
                     res.headers["content-type"].should.contains('application/json');
                     done();
                  });
        });

        it("should get login user", (done) => {
             chai.request(app)
                 .post(`/login`)
                 .set('content-type', 'application/json')
                 .send({
                        "username": "sampleAdmin",
                        "password":"sampleAdmin"
                    })
                 .end((err, res) => {
                     res.should.have.status(200);
                     res.body.should.be.a('object')
                     res.headers["content-type"].should.contains('application/json');

                     done();
                  });
             
        });

        it("should get error in login user", (done) => {
             chai.request(app)
                 .post(`/login`)
                 .set('content-type', 'application/json')
                 .send({
                        "username": "sampleAdmin",
                        "password":"sampleAdmin123"
                    })
                 .end((err, res) => {
                     res.should.have.status(400);
                     res.body.should.be.a('object')
                     res.headers["content-type"].should.contains('application/json');

                     done();
                  });
             
        });

        it("should get Rick&Morty' characters", (done) => {

             chai.request(app)
                 .get(`/characters`)
                 .end((err, res) => {
                     res.should.have.status(200);
                     res.body.should.be.a('array')
                     res.headers["content-type"].should.contains('application/json');
                     
                     const userId = md5("sampleAdmin");
                     client.del(userId);
                     client.del('current_user');

                     done();
                  });           
             
        });
    });
});