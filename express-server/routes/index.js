const express = require('express');

const router = express.Router();

const userController = require('../controllers/userController');
const apiController = require('../controllers/apiController');

/*
GET Pagina index.
*/
router.get(
  '/',
  userController.getIndex,
);

/*
GET current_user.
*/
router.get(
  '/isAuthenticated',
  userController.getIndex,
);

/*
GET login.
*/
router.post(
  '/login',
  userController.validarLogin,
  userController.login,
);

/*
Ruta para añadir un nuevo usuario.
*/
router.post(
  '/users',
  userController.validarInput,
  userController.addUser,
);


/*
Ruta para obtener usuario por id.
*/
router.get(
  '/user/:userId',
  userController.getUser,
);

/*
Ruta para borrar usuario por id.
*/
router.delete(
  '/user/:userId',
  userController.checkUserExists,
  userController.deleteUser,
);

/*
Ruta para obtener a todos los usuarios
*/
router.get(
  '/users',
  userController.getUsers,
);

/*
Ruta para obtener los datos de la API de Rick and Morty
*/
router.get(
  '/characters',
  userController.isAuthenticated,
  apiController.getRickMorty
);

module.exports = router;