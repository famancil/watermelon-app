const redis = require('redis');

// Descomentar cuando se vaya a ejecutar docker compose
const client = redis.createClient({
    host: process.env.REDIS_URL,
    port: 6379
});

//const client = redis.createClient();

const md5 = require('md5');
const async = require('async');

/*
Función para ver la ruta /
*/
exports.getIndex = (req, res) => {
  res.json({ status: 200, message: 'Bienvenido al Servidor API REST' });
};

/*
Función para manejar la ruta del Login
*/
exports.login = (req, res, next) => {

  const user = req.body;
  user.id = md5(user.username);

  //Chequear que el usuario existe
  client.hgetall(user.id, (err, userResult) => {
    if (err) {
      return res.status(400).json({ status: 400, message: 'Existe un error', err });
    }
    if (!userResult) {
      return res.status(400).json({ status: 400, message: 'Usuario no encontrado' });
    }
    if(userResult.password !== md5(user.password))
      return res.status(400).json({ status: 400, message: 'Password Erroneo' });

    //Guardar la variable sesión en Redis
    client.hset(
      "current_user", [
        'userId', userResult.userId,
        'username', userResult.username,
        'email', userResult.email,        
        'password', userResult.password,
      ]
    );

    //Se setea la sesión valida por un tiempo de 2 minutos
    client.expire("current_user", 60*2);
    return res.json(userResult);
  });
};

/*
Función para validar los valores del login.
*/

exports.validarLogin = (req, res, next) => {
  req.checkBody('username', 'Username es requerido').notEmpty();
  req.checkBody('password', 'Password es requerido').notEmpty();

  const errors = req.validationErrors();
  if (errors) {
    res.json({ status: 400, errors, message: errors[0].msg });
    return;
  }
  next();
};

/*
Función para validar los valores del formulario de registro.
*/

exports.validarInput = (req, res, next) => {

  req.checkBody('username', 'Username es requerido').notEmpty();
  req.checkBody('email', 'El email no es valido').isEmail();
  req.sanitizeBody('email').normalizeEmail({
    remove_dots: false,
    remove_extension: false,
    gmail_remove_subaddress: false,
  });
  req.checkBody('password', 'Password es requerido').notEmpty();

  const errors = req.validationErrors();
  if (errors) {
    res.json({ status: 400, errors, message: errors[0].msg });
    return;
  }
  next();
};


/*
Función para registrar usuarios.
*/
exports.addUser = (req, res) => {

  const newUser = req.body;
  newUser.id = md5(newUser.username);
  newUser.password= md5(newUser.password);

  //Chequear que el usuario existe
  client.exists(newUser.id, (err, reply) => {
    if (reply === 1) {
      return res.status(400).json({ status: 400, message: 'Username ya existe, favor escribir otro', newUser });
    }
    

    client.hset(
      newUser.id, [
        'userId', newUser.id,
        'username', newUser.username,
        'email', newUser.email,        
        'password', newUser.password,
      ]
      , (error, result) => {
        if (error) {
          return res.status(400).json({ status: 400, message: 'Se produjo un error', error });
        }
        return res.json({
          result, status: 200, message: 'Usuario creado exitosamente', newUser,
        });
      },
    );
  });
};

/*
Función para obtener a todos los usuarios (a excepción del usuario logeado)
*/
exports.getUsers = (req, res) => {

  client.keys('*', (err, keys) => {
    if (err) {
      return res.status(400).json({ status: 400, message: 'No es posible obtener usuarios', err });
    }
    if (keys) {
      async.map(keys, (key, cb) => {
        client.hgetall(key, (error, value) => {
          if (error) return res.status(400).json({ status: 400, message: 'Se produjo un error', error });
          if(key !== 'current_user'){
            const user = {};
            user.userId = key;
            user.data = value;
            cb(null, user);
          }
        });
      }, (error, users) => {
        if (error) return res.status(400).json({ status: 400, message: 'Se produjo un error', error });
        res.json(users);
      });
    }
  });
};


/*
Función para obtener un usuario por userId
*/
exports.getUser = (req, res) => {
  const { userId } = req.params;
  client.hgetall(userId, (err, user) => {
    if (err) {
      return res.status(400).json({ status: 400, message: 'Se produjo un error', err });
    }
    return res.json(user);
  });
};

/*
Función para ver si existe una sesión valida del usuario.
*/
exports.isAuthenticated = (req, res, next) => {
  client.hgetall("current_user", (err, user) => {
    if (err) {
      return res.status(400).json({ status: 400, message: 'Se produjo un error', err });
    }
    if (!user) {
      return res.json(user);
    }
    next();
  });
};


/*
Función para chequear si usuaro existe
*/
exports.checkUserExists = (req, res, next) => {
  const { userId } = req.params;
  client.hgetall(userId, (err, user) => {
    if (err) {
      return res.status(400).json({ status: 400, message: 'Se produjo un error', err });
    }
    if (!user) {
      return res.json({ status: 400, message: 'Usuario no encontrado' });
    }
    next();
  });
};

/*
Función para borrar un usuario por userId
*/
exports.deleteUser = (req, res) => {
  const { userId } = req.params;
  client.del(userId, (err, result) => {
    if (err) {
      return res.status(400).json({ status: 400, message: 'Se produjo un error', err });
    }
    return res.json({ status: 200, message: 'Usuario eliminado exitosamente', result });
  });
};

