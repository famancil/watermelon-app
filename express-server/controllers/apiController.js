var request = require('request');

/*
Función para consumir la API de Rick & Morty*/
exports.getRickMorty = (req, res) => {
  request('https://rickandmortyapi.com/api/character/', function (error, response, body) {
    if (!error && response.statusCode == 200) {
        let resultado = JSON.parse(body);

        //Se borran algunos valores que no son pedidos
        for(let index in resultado.results) {
          delete resultado.results[index]["id"]
          delete resultado.results[index]["type"]
          delete resultado.results[index]["origin"]
          delete resultado.results[index]["location"]
          delete resultado.results[index]["episode"]
          delete resultado.results[index]["url"]
          delete resultado.results[index]["created"]
        }
        res.json(resultado.results)
     }
  });
};