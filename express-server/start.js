const redis = require('redis');

require('dotenv').config();


// Descomentar cuando se vaya a ejecutar docker compose
const redisClient = redis.createClient({
   host: process.env.REDIS_URL,
   port: 6379
});

//const redisClient = redis.createClient();

redisClient.on('connect', () => {
  console.log('##########################################################');
  console.log('#####            REDIS STORE CONNECTED               #####');
  console.log('##########################################################\n');
});

redisClient.on('error', (err) => {
  console.log(`Redis error: ${err}`);
});

// Start our app!
const app = require('./app');

// Start Server
app.set('port', process.env.PORT || 7777);
const server = app.listen(app.get('port'), () => {
  console.log('##########################################################');
  console.log('#####               STARTING SERVER                  #####');
  console.log('##########################################################\n');
  console.log(`Express running → PORT ${server.address().port}`);
});

module.exports = app;