# watermelon-app
Desarrollado por Felipe Mancilla S - famancil.inf@gmail.com
Diciembre 2019

# Introducción

Este sistema es para mostrar los datos de los personajes de la serie Rick&Morty. Se compone de dos partes: Backend (API REST) y Frontend. La parte Backend esta desarrollada en NodeJS (v12.8.0) y ExpressJS (v4.17.1), utilizando la imagen de Docker "Redis" para la base de datos. La parte Frontend esta hecho en ReactJS (v16.12.0). 

<br>

## Parte Backend

La estructura del servidor API REST, es:

* Controllers: Donde se existen dos controladores, uno para manejar las rutas para gestión de usuarios (crear, loguear, obtener y borrar), userController. Y otro para manejar la ruta para comunicarse con la API de Rick&Morty, apiController.
Para userController, estan las rutas para registrar un usuario, logearse, validar si los datos son validos, chequear si existe el usuario y/o que el usuario este logeado. Y finalmente, borrar un usuario.
Para apiController, esta la ruta para comunicarse con el servidor de Rick&Morty y solo entrar los valores pedidos en el arreglo.

* Routes: Donde esta el archivo index.js con todas las rutas que maneja el servidor.

* Tests: Donde esta el archivo test.js con las pruebas hechas a cada módulo pedido en esta prueba técnica.

* App.js: El archivo con todas las configuraciones para NodeJS y ExpressJS.

* Start.js: El archivo para levantar el servidor de NodeJS.

## Rutas.

Las rutas del servidor son,

* GET /: Ruta Index.
* GET /users: Obtener todos los usuarios.
* GET /users/userId: Obtener un usuario con id 'userId'. 
* POST /users: Añadir un usuario con username,email y password.
* DELETE /users/userId: Eliminar un usuario con id 'userId'. 
* POST /login: Loguear como 'current_user' y guardar su sesión por 2 minutos.
* GET /isAuthenticated: Chequear si existe 'current_user', sino devolver a la ruta "login" del navegador.
* GET /characters: Validar si existe 'current_user', si es así entonces entregar un arreglo de los personajes de Rick&Morty.


## Pruebas

En la carpeta "Tests", en el archivo test.js, se encuentran las pruebas hechas para cada ruta pedida en esta prueba. 
Para probar todo, ejecute el comando:

~~~sh
git clone git@gitlab.com:famancil/watermelon-app.git
cd watermelon-app/express-server/
sudo docker run -d --name redis_test -p 6379:6379 redis redis-server
npm test
~~~

Favor realizar esta prueba antes de ejecutar docker-compose. Y para saber el "% test coverage stmts", ejecuta el sgte comando: 

~~~sh
git clone git@gitlab.com:famancil/watermelon-app.git
cd watermelon-app/express-server/
sudo docker run -d --name redis_test -p 6379:6379 redis redis-server
npm run test-coverage
~~~

## Parte Frontend

La estructura del frontend, es:

* App: Donde existen todos los archivos que manejan las rutas en el navegador.
Como index.js, el archivo principal (visualizado en index.html) que hara la conexión con users.js y login.js para la visualización de Login y mostrar la tabla con los datos de Rick&Morty.

* Dev: Donde se encuentra el archivo nginx.conf que servira para configurar Nginx y redirrecionar las consultas al cliente Frontend.

* Dist: Donde se encuentra los archivos que toma Nginx para mostrar en el servidor.

* Public: Donde se guarda los archivos estaticos.

## Rutas.

Las rutas de la pagina es,

* GET /: Para un usuario autenticado, es posible ver la tabla con los datos de los personajes de Rick&Morty, con los datos pedidos.
* GET /login: Mostrar la vista del login.

## Pruebas

Para chequear la sintaxis mediante el linter, se ejecuta el comando: 

~~~sh
git clone git@gitlab.com:famancil/watermelon-app.git
cd watermelon-app/react-app
npm run lint
~~~

Favor realizar esta prueba antes de ejecutar docker-compose.

# Consideraciones

Antes de compilar docker-compose, favor es requerido que hagan los test de la parte backend y frontend (Descrito anteriormente).

~~~sh
git clone git@gitlab.com:famancil/watermelon-app.git

cd watermelon-app/express-server/
sudo docker run -d --name redis_test -p 6379:6379 redis redis-server
npm test
npm run test-coverage

cd ../react-app
npm run lint
~~~

Ya que si bien se agregaron al gitlab-ci.yml, no fue posible compilar las pruebas de express-server (pero si esta las de react-app, viendo el apartado de pipelines del gitlab).

Una vez hecho las pruebas, ejecute el docker-compose:

~~~sh
git clone git@gitlab.com:famancil/watermelon-app.git

cd watermelon-app/express-server/
docker-compose up -d --build
~~~

Y de esa manera estarian las imagenes y containers levantados, y listo para probar. Favor esperar de 10 a 15 segundos para que los servidores en los contenedores esten subidos exitosamente.

* Para Backend, la URL para acceder a la API REST es

~~~sh
localhost:7777
~~~

* Para Frontend, la URL para acceder a la API REST es

~~~sh
localhost:8080
~~~

# Camino a Seguir

Antes de empezar con la pagina, es necesario probar (como usuario), registrar un usuario. Para esto (utilizando Postman) se ingresa al servidor API REST, y se añade en el cuerpo, los valores para el usuario (email,username y password).

~~~sh
## Metodo para añadir un usuario
POST localhost:7777/users

## JSON
{
	"username": "sample1",
	"email": "sample1@gmail.com",
	"password":"sample12"
}
~~~

Deberia entregar un resultado así,

~~~sh
## Resultado
{
    "result": 4,
    "status": 200,
    "message": "Usuario creado exitosamente",
    "newUser": {
        "username": "sample1",
        "email": "sample1@gmail.com",
        "password": "f7cfd52eb9a92c4b68638bc08fa5c682",
        "id": "ac46374a846d97e22f917b6863f690ad"
    }
}
~~~

Ya listo, esto se abre un navegador (de preferencia Google Chrome) y se accede a la pagina "localhost:8080".
Supuestamente, como querra mostrar la tabla, primero revisara si existe un usuario logueado y como no es así, devuelve a la ruta "localhost:8080/login".

Una vez que se logue con los datos creados anteriomente, se podra observar la tabla con los personajes.

Si esperan 2 minutos despues de iniciar sesión, al terminar el tiempo, favor volver a cargar la pagina donde se encuentra la tabla con los personajes.