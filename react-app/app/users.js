import React, { Component } from 'react';
import Table from 'react-bootstrap/Table';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import PropTypes from 'prop-types';

class Users extends Component {


	constructor(props) {
	super(props);
	this.state = { users: [] }
	}

	componentDidMount() {
  fetch('http://localhost:7777/characters')
  .then(res => res.json())
  .then((data) => {
  if(!data) this.props.history.push('/login')
  else this.setState({ users: data })
  })
  .catch(console.log)
	}

  render() {
    const items = this.state.users;
    if(!items) return null;
    const listItems = items.map((item,i) =>
      <tr key={i} align="center">
        <td><img src={item.image} /></td>
        <td scope="row">{item.name}</td>
        <td>{item.gender}</td>
        <td>{item.species}</td>
        <td>{item.status}</td>
      </tr>
      

      
      
    );
    return (

        <div>

        <h1 style={{textAlignVertical: "center",textAlign: "center"}}>Rick & Morty&apos;s Characters </h1>
        <Row className="justify-content-md-center">
        <Col sm={10} >
<Table striped bordered hover>
  <thead>
    <tr align="center">
      <th scope="col">Img</th>
      <th scope="col">Name</th>
      <th scope="col">Gender</th>
      <th scope="col">Species</th>
      <th scope="col">Status</th>
    </tr>
  </thead>
  <tbody className="items">
    {listItems}
  </tbody>
</Table>
			</Col>
			</Row>        	
      </div>

    );
  }
}

Users.propTypes = {
  history: PropTypes.any,
};

export default Users;