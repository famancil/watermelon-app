import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import {
  Route,
  BrowserRouter as Router,
  Switch
} from "react-router-dom";
import Users from "./users";
import Login from "./login";
import Notfound from "./notfound";
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from 'react-bootstrap/Navbar';

const routing = (
  <Router>    
	<Navbar bg="light" expand="lg">
	<Navbar.Brand href="#home"><img src={'../images/watermelon.png'} style={{ width: 250 }}/></Navbar.Brand>
	<Navbar.Toggle aria-controls="basic-navbar-nav" />
	</Navbar>
	
	<Switch>
        <Route exact path="/" component={Users} />
        <Route path="/login" component={Login} />
        <Route component={Notfound} />
    </Switch>

  </Router>
);

ReactDOM.render(routing, document.getElementById("root"));