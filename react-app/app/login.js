import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Jumbotron from 'react-bootstrap/Jumbotron';

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      error: '',
    };

    this.handlePassChange = this.handlePassChange.bind(this);
    this.handleUserChange = this.handleUserChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.dismissError = this.dismissError.bind(this);
  }

  dismissError() {
    this.setState({ error: '' });
  }

  handleSubmit(evt) {
    evt.preventDefault();

    if (!this.state.username) {
      return this.setState({ error: 'Username es requerido' });
    }

    if (!this.state.password) {
      return this.setState({ error: 'Password es requerido' });
    }

    fetch('http://localhost:7777/login', {
      method: "post",
      headers: new Headers({
       'Content-Type': 'application/json'
      }), 
      body: JSON.stringify({username: this.state.username,
        password: this.state.password})
    })
      .then(res => res.json())
      .then((data) => {
        if(data && data.status === 400){
          this.setState({
            error: "Usuario no existe, favor registrese.",
            username: "",
            password: ""
          });
        }
        if(data && !data.status) this.props.history.push("/")
        
      })
      .catch(console.log)
  }

  handleUserChange(evt) {
    this.setState({
      username: evt.target.value,
    });
  };

  handlePassChange(evt) {
    this.setState({
      password: evt.target.value,
    });
  }

  render() {

    return (<div className="Login" align="center">
     <br/>
     <Row className="justify-content-md-center">
      <Col sm={4} >
      <Jumbotron>
      <img src={'../images/watermelonlogo.png'} style={{ width: 110 }}/>
      <br/>
      <h1>Login</h1>
      <Form onSubmit={this.handleSubmit} align="center">
         {
            this.state.error &&
            <h3 data-test="error" onClick={this.dismissError}>
              <button onClick={this.dismissError}>✖</button>
              {this.state.error}
            </h3>
          }
        <Form.Group controlId="formBasicUsername">
          <Form.Label>Username</Form.Label>
          <Form.Control type="text" placeholder="Enter Username" value={this.state.username} onChange={this.handleUserChange}/>
        </Form.Group>
        <Form.Group controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="Enter Password" data-test="password" value={this.state.password} onChange={this.handlePassChange} />
        </Form.Group>
        <Button variant="primary" type="submit">
          Submit
        </Button>
      </Form>
      </Jumbotron>
      </Col>
     </Row>    
    </div>

    );
  }
}

Login.propTypes = {
  history: PropTypes.any,
};

export default Login;